package org.egreen.opensms.server.dao;

import org.egreen.opensms.server.entity.Department;
import org.egreen.opensms.server.entity.Department;

import java.util.List;

/**
 * Created by Pramoda Fernando on 8/19/2014.
 */
public interface DepartmentDAOController extends DAOController<Department,Long> {
    List<Department> findDepartmentByQuery(String categoryname);

    List<Department> findAllDepartment();

    List<Department> findNonParent();

    int deleteDepartment(Long categoryId);

}
