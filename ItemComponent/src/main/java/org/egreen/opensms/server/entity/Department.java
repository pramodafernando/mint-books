package org.egreen.opensms.server.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * Created by dewmal on 8/19/14.
 */
@Entity
@Table(name = "department")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Department implements EntityInterface<Long> {
    private Long departmentId;
    private String departmentName;


    @Id
    @Column(name = "department_id", nullable = false, insertable = true, updatable = true)
    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }


    @Basic
    @Column(name = "department_name")
    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Department department = (Department) o;

        if (departmentId != null ? !departmentId.equals(department.departmentId) : department.departmentId != null) return false;
        if (departmentName != null ? !departmentName.equals(department.departmentName) : department.departmentName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = departmentId != null ? departmentId.hashCode() : 0;
        result = 31 * result + (departmentName != null ? departmentName.hashCode() : 0);
        return result;
    }

    @Override
    @Transient
    public Long getId() {
        return getDepartmentId();
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentId=" + departmentId +
                ", departmentName='" + departmentName + '\'' +
                '}';
    }
}
