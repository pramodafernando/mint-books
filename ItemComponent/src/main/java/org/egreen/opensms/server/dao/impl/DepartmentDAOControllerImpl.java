package org.egreen.opensms.server.dao.impl;

import org.egreen.opensms.server.dao.DepartmentDAOController;
import org.egreen.opensms.server.entity.Department;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Pramoda Fernando on 8/19/2014.
 */
@Repository
public class DepartmentDAOControllerImpl extends AbstractDAOController<Department,Long> implements DepartmentDAOController {

    public DepartmentDAOControllerImpl() {
        super(Department.class, Long.class);
    }


    @Override
    public List<Department> findDepartmentByQuery(String categoryname) {


        //getSession().createQuery("SELECT u from Department u WHERE u.categoryByParentDepartment")

       // Criteria criteria = getSession().createCriteria(entityType).createCriteria();
      //  criteria.add(Restrictions.like("category", categoryname, MatchMode.ANYWHERE));



        return null;// criteria.list();
    }

    @Override
    public List<Department> findAllDepartment() {
        Criteria criteria = getSession().createCriteria(entityType);
        //criteria.setFirstResult(5);
        //criteria.setMaxResults(10);
        System.out.println("LIST:"+criteria.list());
        return criteria.list();
    }

    @Override
    public List<Department> findNonParent() {
        Criteria criteria = getSession().createCriteria(entityType);
        criteria.add(Restrictions.isNull("categoryByParentDepartment"));
        return criteria.list();
    }

    @Override
    public int deleteDepartment(Long categoryId) {
        //Integer categoryId = read.getDepartmentId();
        Session session=getSession();
        String hql = "delete from Department where categoryId= :categoryId";
        int i = session.createQuery(hql).setLong("categoryId", categoryId).executeUpdate();
        return i;
    }




}
