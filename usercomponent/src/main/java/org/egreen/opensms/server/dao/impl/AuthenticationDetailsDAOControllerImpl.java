package org.egreen.opensms.server.dao.impl;

import org.egreen.opensms.server.dao.AuthenticationDetailsDAOController;

import org.egreen.opensms.server.entity.AuthenticationDetails;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * Created by Pramoda Fernando on 1/27/2015.
 */

@Repository
public class AuthenticationDetailsDAOControllerImpl extends AbstractDAOController<AuthenticationDetails,String> implements AuthenticationDetailsDAOController {
    public AuthenticationDetailsDAOControllerImpl() {
        super(AuthenticationDetails.class, String.class);
    }

    @Override
    public Integer check_email_validity(String email) {
        Criteria criteria = getSession().createCriteria(entityType);
        criteria.add(Restrictions.eq("email", email));
        Object o = criteria.uniqueResult();
        if (o != null) {
            return 1;
        }else{
            return 0;
        }
    }

    @Override
    public AuthenticationDetails searchUserByEmail(String email) {
        Criteria criteria = getSession().createCriteria(entityType);
        criteria.add(Restrictions.eq("email", email));
        AuthenticationDetails authenticationDetails = (AuthenticationDetails) criteria.uniqueResult();
        System.out.println("DB : "+authenticationDetails);
        if (authenticationDetails != null) {
            return authenticationDetails;
        }else{
            return null;
        }
    }

    @Override
    public AuthenticationDetails login(String email, String userPassword) {
        Criteria criteria = getSession().createCriteria(entityType);
        criteria.add(Restrictions.eq("email", email));
        criteria.add(Restrictions.eq("password", userPassword));
        AuthenticationDetails o = (AuthenticationDetails)criteria.uniqueResult();
        if (o != null) {
            return o;
        }else{
            return null;
        }
    }

    @Override
    public AuthenticationDetails reset_password(String email) {
        Criteria criteria = getSession().createCriteria(entityType);
        criteria.add(Restrictions.eq("email", email));
        return (AuthenticationDetails)criteria.uniqueResult();
    }

    @Override
    public AuthenticationDetails varify_detail(String varification_code, String detail_id) {
        Criteria criteria = getSession().createCriteria(entityType);
        criteria.add(Restrictions.eq("voco_id", varification_code));
        criteria.add(Restrictions.eq("details_id", detail_id));
        return(AuthenticationDetails)criteria.uniqueResult();
    }

    @Override
    public AuthenticationDetails get_all_details_by_email(String email) {
        Criteria criteria = getSession().createCriteria(entityType);
        criteria.add(Restrictions.eq("email", email));
        return (AuthenticationDetails)criteria.uniqueResult();
    }
}
