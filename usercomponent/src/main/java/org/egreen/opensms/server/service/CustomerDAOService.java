package org.egreen.opensms.server.service;


import org.egreen.opensms.server.dao.CustomerDAOController;
import org.egreen.opensms.server.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by dewmal on 7/17/14.
 */
@Service
public class CustomerDAOService {

    @Autowired
    private CustomerDAOController userContactDetailsDAOController;

    /**
     * Save Contact Detail
     * (POST)
     *
     * @author Pramoda Nadeeshan Fernando
     * @since 2015-02-16 03.26PM
     * @version 1.0
     *
     * @param contactDetail
     * @return
     */
    public Long saveContactDetails(Customer contactDetail) {
        long userId = new Date().getTime();
        contactDetail.setUserId(userId);


        return userContactDetailsDAOController.create(contactDetail);
    }

    /**
     * Get user contact details
     *
     * @param userid
     * @return
     */
    public Customer getContactDetails(Long userid) {
        return userContactDetailsDAOController.read(userid);
    }


    /**
     *
     * Get User By City
     *
     * @param city
     * @return
     */
    public List<Customer> getCustomersbyCity(String city) {

        List<Customer> userByCity = userContactDetailsDAOController.getUserByCity(city);

        return userByCity;

    }

    public List<Customer> searchAllDetails(Integer limit,Integer offset) {
        return userContactDetailsDAOController.getAll(offset,limit,"userId");
    }


    public Long getRowCount() {
        return userContactDetailsDAOController.getAllCount();
    }

    public void updateContactDetails(Customer userContactDetail) {
        userContactDetailsDAOController.update(userContactDetail);
    }

    public Customer searchAllDetailsByUserId(Long userId) {
        return userContactDetailsDAOController.read(userId);
    }

    /**
     * Search All Customer
     * (GET)
     *
     * @author Pramoda Nadeeshan Fernando
     * @since 2015-02-16 03.26PM
     * @version 1.0
     *
     * @return
     */
    public List<Customer> searchAllContactDetails() {
        return userContactDetailsDAOController.getAll();
    }

    /**
     * Search All Sort By Customer Name
     * (GET)
     *
     * @author Pramoda Nadeeshan Fernando
     * @since 2015-02-16 03.26PM
     * @version 1.0
     *
     * @return
     */
    public List<Customer> search_all_sortByCustomerName() {
        return userContactDetailsDAOController.search_all_sortByCustomerName();

    }

    /**
     * Search All Sort By CustomerOrder Value
     * (GET)
     *
     * @author Pramoda Nadeeshan Fernando
     * @since 2015-02-16 03.26PM
     * @version 1.0
     *
     *
     * @return
     */
    public List<Customer> search_all_sortByCustomerOrderValue() {

        return userContactDetailsDAOController.search_all_sortByCustomerOrderValue();
    }

    public List<Customer> testing_search_all() {
        return userContactDetailsDAOController.testing_search_all();
    }
}
