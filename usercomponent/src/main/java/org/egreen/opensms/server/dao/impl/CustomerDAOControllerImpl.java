package org.egreen.opensms.server.dao.impl;

import org.egreen.opensms.server.dao.CustomerDAOController;

import org.egreen.opensms.server.entity.Customer;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by dewmal on 7/17/14.
 */
@Repository
public class CustomerDAOControllerImpl extends AbstractDAOController<Customer, Long> implements CustomerDAOController {

    public CustomerDAOControllerImpl() {
        super(Customer.class, Long.class);
    }


    @Override
    public List<Customer> findUsers(String searchQuery) {
        System.out.println(searchQuery);
        Criteria criteria = getSession().createCriteria(entityType);
        Disjunction or = Restrictions.disjunction();
        or.add(Restrictions.like("name", searchQuery, MatchMode.ANYWHERE));
        or.add(Restrictions.like("province", searchQuery, MatchMode.ANYWHERE));
        or.add(Restrictions.like("addressLine1", searchQuery, MatchMode.ANYWHERE));
        or.add(Restrictions.like("addressLine2", searchQuery, MatchMode.ANYWHERE));
        or.add(Restrictions.like("country", searchQuery, MatchMode.ANYWHERE));
        or.add(Restrictions.like("email", searchQuery, MatchMode.ANYWHERE));
        criteria.add(or);
        return criteria.list();
    }

    public List<Customer> getUserByCity(String city){
        Criteria criteria = getSession().createCriteria(entityType);
        criteria.add(Restrictions.like("city", city));
        System.out.println(criteria.list());
        return criteria.list();
    }

    @Override
    public List<Customer> getAllCustomerbyName(String custName) {
        Criteria criteria = getSession().createCriteria(entityType);
        criteria.add(Restrictions.like("name",custName,MatchMode.ANYWHERE));
        return criteria.list();
    }

    @Override
    public List<Customer> search_all_sortByCustomerName() {
        Criteria criteria = getSession().createCriteria(entityType);
        criteria.addOrder(Order.asc("firstName"));
        return criteria.list();
    }

    @Override
    public List<Customer> search_all_sortByCustomerOrderValue() {
        Session session=getSession();
        String hql = "SELECT u FROM UserContactDetail u, CustomerOrder cu group by user_id order by amount desc";
        return session.createQuery(hql).list();

    }

    @Override
    public List<Customer> testing_search_all() {
        Session session=getSession();
        String hql = "EXPLAIN SELECT u FROM UserContactDetail u";
        return session.createQuery(hql).list();
    }


}
