package org.egreen.opensms.server.controller;

import org.apache.commons.io.IOUtils;
import org.egreen.opensms.server.entity.AuthenticationDetails;

import org.egreen.opensms.server.service.AuthenticationController;
import org.egreen.opensms.server.service.AuthenticationDetailsDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;


/**
 * Created by dewmal on 7/17/14.
 */
@Controller
@RequestMapping("mintbooks/v1/authentication_details/")
public class  AuthenticationDetailsController {




    @Autowired
    private AuthenticationDetailsDAOService authenticationDetailsDAOService;

    @Autowired
    private AuthenticationController authenticationController;



    /**
     *
     * User SignUp
     * Sends Verification Email To User
     *
     * @author Pramoda Nadeeshan Fernando
     * @since 2015-02-12 4.26PM
     * @version 1.0
     *
     * @param authenticationDetails
     * @return
     */
    @RequestMapping(value = "signup", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public ResponseMessage signup(@RequestBody AuthenticationDetails authenticationDetails) {
        String aLong = authenticationDetailsDAOService.signup(authenticationDetails);
        ResponseMessage responseMessage = ResponseMessage.SUCCESS;
        responseMessage.setData(aLong);
        return ResponseMessage.SUCCESS;
    }

//    /**
//     *
//     * User Update
//     *
//     * @param user
//     * @return
//     */
//    @RequestMapping(value = "update_user", method = RequestMethod.POST, headers = "Accept=application/json")
//    @ResponseBody
//    public ResponseMessage edit_user(@RequestBody Attributes user) {
//        Long aLong = userDAOService.edit_user(user);
//        ResponseMessage responseMessage = ResponseMessage.SUCCESS;
//        responseMessage.setData(aLong);
//        return responseMessage;
//    }

/////////                                                                                                       ////////
///////////////////////////////////////////////////GET CONTROLLERS//////////////////////////////////////////////////////
/////////                                                                                                       ////////

    /**
     *
     * User Login
     *
     * @author Pramoda Nadeeshan Fernando
     * @since 2015-02-12 4.26PM
     * @version 1.0
     *
     * @param username
     * @param password
     * @return
     */
    @RequestMapping(value = "login", method = RequestMethod.POST)
    @ResponseBody
    public ResponseMessage login(@RequestParam("email") String username, @RequestParam("password") String password) {
        AuthenticationDetails res = authenticationDetailsDAOService.login(username, password);
        ResponseMessage responseMessage;
        if(res != null){
            responseMessage = ResponseMessage.SUCCESS;
            responseMessage.setData(res);
        }else{
            responseMessage = ResponseMessage.DANGER;
            responseMessage.setData(res);
        }
        return responseMessage;
    }

    /**
     *
     * Check Email Validity
     *
     * @author Pramoda Nadeeshan Fernando
     * @since 2015-02-12 4.26PM
     * @version 1.0
     *
     * @param email
     * @return
     */
    @RequestMapping(value = "check_email_validity", method = RequestMethod.POST)
    @ResponseBody
    public ResponseMessage check_email_validity(@RequestParam("email") String email) {
        Integer res = authenticationDetailsDAOService.check_email_validity(email);
        ResponseMessage responseMessage;
        if(res != null){
            responseMessage = ResponseMessage.SUCCESS;
            responseMessage.setData(res);
        }else{
            responseMessage = ResponseMessage.DANGER;
            responseMessage.setData(res);
        }
        return responseMessage;
    }

    /**
     *
     * Replace New Password
     * Sends New Password To User
     *
     * @author Pramoda Nadeeshan Fernando
     * @since 2015-02-12 4.26PM
     * @version 1.0
     *
     * @param email
     * @return
     */
    @RequestMapping(value = "reset_password", method = RequestMethod.POST)
    @ResponseBody
    public ResponseMessage reset_password(@RequestParam("email") String email) {
        Integer res = authenticationDetailsDAOService.reset_password(email);
        ResponseMessage responseMessage;
        if(res != null){
            responseMessage = ResponseMessage.SUCCESS;
            responseMessage.setData(res);
        }else{
            responseMessage = ResponseMessage.DANGER;
            responseMessage.setData(res);
        }
        return responseMessage;
    }

    /**
     *
     * Varify User Details
     *
     * @author Pramoda Nadeeshan Fernando
     * @since 2015-02-12 4.26PM
     * @version 1.0
     *
     * @param varification_code
     * @param detail_id
     * @return
     *
     */
//    @RequestMapping(value = "varify_details", method = RequestMethod.GET)
//    public String varify_detail(@RequestParam("varification_code") String varification_code, @RequestParam("detail_id") String detail_id) {
//        String res = authenticationDetailsDAOService.varify_detail(varification_code, detail_id);
//        return res;
//    }

    /**
     *
     * Get All AuthenticationDetails Details By Detail Id
     *
     * @author Pramoda Nadeeshan Fernando
     * @since 2015-02-12 4.26PM
     * @version 1.0
     *
     * @param detail_id
     * @return
     *
     */
    @RequestMapping(value = "get_all_user_details_by_userId", method = RequestMethod.POST)
    @ResponseBody
    public AuthenticationDetails get_all_details_byUserId(@RequestParam("detail_id") String detail_id) {
        AuthenticationDetails res = authenticationDetailsDAOService.get_all_details_byUserId(detail_id);
        return res;
    }

    /**
     *
     * Get All User Details Model By Email
     *
     * @author Pramoda Nadeeshan Fernando
     * @since 2015-02-12 4.26PM
     * @version 1.0
     *
     * @param email
     * @return
     *
     */
//    @RequestMapping(value = "getAllUserDetailsModelByEmail", method = RequestMethod.POST)
//    @ResponseBody
//    public ResponseMessage get_all_details_by_email(@RequestParam("email") String email) {
//        AuthenticationDetails authenticationDetails = authenticationDetailsDAOService.get_all_details_by_email(email);
//
//        UserDetails userDetails =  userDetailsDAOService.getDetailsByVocoId(authenticationDetails.getVoco_id());
//        ContactDetails contactDetails = contactDetailsDAOService.getDetailsByVocoId(authenticationDetails.getVoco_id());
//
//        UserDetailsModel userDetailsModel = new UserDetailsModel();
//        userDetailsModel.setAuthenticationDetails(authenticationDetails);
//        userDetailsModel.setContactDetails(contactDetails);
//        userDetailsModel.setUserDetails(userDetails);
//
//        ResponseMessage responseMessage = ResponseMessage.SUCCESS;
//        responseMessage.setData(userDetailsModel);
//        return ResponseMessage.SUCCESS;
//
//    }

    /**
     *
     * Ob
     *
     * @author Pramoda Nadeeshan Fernando
     * @since 2015-02-12 4.26PM
     * @version 1.0
     *
     * @return
     */
    @RequestMapping(value = "ob", method = RequestMethod.GET)
    @ResponseBody
    public AuthenticationDetails getob() {
return new AuthenticationDetails();
    }






}