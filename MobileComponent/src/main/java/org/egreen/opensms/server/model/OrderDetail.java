package org.egreen.opensms.server.model;

/**
 * Created by Pramoda Fernando on 12/18/2014.
 */
public class OrderDetail {
    private long orderId;
    private long gsrOrderId;
    private String customerName;
    private long datetime;
    private double amount;
    private int numberOfItems;


    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getDatetime() {
        return datetime;
    }

    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getNumberOfItems() {
        return numberOfItems;
    }

    public void setNumberOfItems(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public long getGsrOrderId() {
        return gsrOrderId;
    }

    public void setGsrOrderId(long gsrOrderId) {
        this.gsrOrderId = gsrOrderId;
    }

    @Override
    public String toString() {
        return "OrderDetail{" +
                "orderId=" + orderId +
                ", gsrOrderId=" + gsrOrderId +
                ", customerName='" + customerName + '\'' +
                ", datetime=" + datetime +
                ", amount=" + amount +
                ", numberOfItems=" + numberOfItems +
                '}';
    }
}
