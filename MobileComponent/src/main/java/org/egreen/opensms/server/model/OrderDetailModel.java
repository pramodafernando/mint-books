package org.egreen.opensms.server.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pramoda Fernando on 12/18/2014.
 */
public class OrderDetailModel extends OrderDetail {

    private List<ItemStockModel> itemModelList = new ArrayList<ItemStockModel>();


    public List<ItemStockModel> getItemModelList() {
        return itemModelList;
    }

    public void setItemModelList(List<ItemStockModel> itemModelList) {
        this.itemModelList = itemModelList;
    }

    @Override
    public String toString() {

        return super.toString() + "OrderDetailModel{" +
                "itemModelList=" + itemModelList +
                '}';
    }
}
